public class App {
    public static void main(String[] args) throws Exception {
        OriginalClass originalClass = new OriginalClass();
        OriginalClass overrideClass = new ClassOverride();

        //Override
        originalClass.method();
        overrideClass.method();
        
        //Overload
        originalClass.method("Method overloaded");
    }
}
